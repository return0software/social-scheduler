package com.gitlab.return0software;

import org.junit.jupiter.api.Test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * GenericTest
 */
public class GenericTest {

    private final Logger log = LoggerFactory.getLogger(GenericTest.class);

    @Test
    public void hashLength() {
        StringBuilder hash = new StringBuilder();
        String password = "Tristan Partin";
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(String.format("%d", System.currentTimeMillis() / 1000L).getBytes());
            md.update(password.getBytes());

            final byte[] bytes = md.digest();
            char[] digits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
            for (byte b : bytes) {
                hash.append(digits[b & 0xc0 >> 4]);
                hash.append(digits[b & 0x0f]);
            }
            System.out.println(hash.toString());
        } catch (NoSuchAlgorithmException e) {
            log.error("{}", e);
        }
        System.out.println();
    }
}
