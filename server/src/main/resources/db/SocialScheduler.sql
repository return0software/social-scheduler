-- Creating Database

DROP DATABASE IF EXISTS SocialScheduler;
CREATE DATABASE SocialScheduler
    CHARACTER SET = "utf8"
    COLLATE = "utf8_general_ci";
USE SocialScheduler;

-- Creating Tables

CREATE TABLE User(
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(32) NOT NULL UNIQUE,
);
