package com.gitlab.return0software;

import com.gitlab.return0software.health.TemplateHealthCheck;
import com.gitlab.return0software.resources.SSApplicationResource;
import com.gitlab.return0software.resources.UserResource;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class SSApplication extends Application<SSConfiguration> {
    public static void main(String[] args) throws Exception {
        new SSApplication().run(args);
    }

    @Override
    public String getName() {
        return "social-scheduler-server";
    }

    @Override
    public void initialize(Bootstrap<SSConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public void run(SSConfiguration config, Environment environ) {
        final SSApplicationResource applicationResource = new SSApplicationResource(
            config.getTemplate(), config.getDefaultName());
        final UserResource userResource= new UserResource();
        final TemplateHealthCheck healthCheck =
            new TemplateHealthCheck(config.getTemplate());
        environ.healthChecks().register("template", healthCheck);
        environ.jersey().register(applicationResource);
        environ.jersey().register(userResource);
    }
}
