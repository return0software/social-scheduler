package com.gitlab.return0software.resources;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.Timed;
import com.gitlab.return0software.api.User;

@Path("/user")
public class UserResource {
    @POST
    @Timed
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser() {
        return Response.status(200).entity(new User(5, "tristan957")).build();
    }
}
