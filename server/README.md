# Social Scheduler Server

## To set up server

Install `Java >= 1.8`

Install `Gradle >= 3.5` *(Packaged in repo)*

### To Build

#### Linux/MacOS

    gradle build

or

    ./gradlew build

#### Windows

    gradlew.bat
