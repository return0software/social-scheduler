import React from 'react';
import { Route } from 'react-router';
import { Home } from './containers';

const routes = [
    {
        path: '/',
        component: Home,
        navVisible: true,
        navText: 'Home',
        exact: true
    },
    {
        path: '/test',
        component: Home,
        navVisible: true,
        navText: 'Test',
        exact: true
    },
];

export const routeComponents = routes.map(({ path, component, exact }, key) => (
    <Route exact={exact} path={path} component={component} key={key} />
));

export const navComponents = routes.filter(({ navVisible }) => (
    navVisible
));
