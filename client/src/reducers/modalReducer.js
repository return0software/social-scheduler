const INITIAL_STATE = null;

const modalReducer = (state = INITIAL_STATE, action ) => {
  switch (action.currentModal) {
    // case 'EXPORT_DATA':
    //   return <ExportDataModal {...props}/>;
    default:
      return null;
  }
};

export default modalReducer;
