import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { NavLink } from '../../components';
import { navComponents } from '../../routes';
import './styles.css';

class Navbar extends Component {
    render() {
        return (
            <ul>
                {navComponents.map(({path, navText}, key) => (
                    <li key={key}><NavLink to={path}>{navText}</NavLink></li> 
                ))}
                <li className='login'><Link to="#about">Login</Link></li>
            </ul>
        );
    }
}

export default connect()(Navbar)