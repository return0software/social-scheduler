This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) by [Joseph Martinsen](https://github.com/JosephMart).

Utilizes [React-Router](https://github.com/ReactTraining/react-router) and [Redux](https://redux.js.org/).

```
React-Template-JMM/
  .vscode/
    launch.json
  README.md
  package.json
  yarn.lock
  .gitignore
  public/
    index.html
    favicon.ico
    manifest.json
  src/
    actions/
      index.js
      redditActions.js
    components/
      Footer.js
      Link.js
      Picker.js
      Posts.js
      Todo.js
      TodoList.js
    containers/
      App/
        App.css
        App.js
        App.test.js
      AddTodo.js
      FilterLink.js
      RedditAsync.js
      VisibleTodoList.js
    reducers/
      index.js
      reddit.js
      todos.js
      visibilityFilter.js
    constants.js
    index.css
    index.js
    logo.svg
    registerServiceWorker.js
    routes.js
```

You can find the most recent version of `create-react-app` guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).
